// @license magnet:?xt=urn:btih:87f119ba0b429ba17a44b4bffcab33165ebdacc0&dn=freebsd.txt FreeBSD
document.getElementById("hamexam").reset();

function process() {
	var score = 0;

	var els = document.querySelectorAll('input:checked');
	for (var i = 0; i < els.length; i++) {
		var a = keys.indexOf(String(els[i].name));
		var label = document.querySelector(`[for="${els[i].id}"]`);

		//console.log(els[i]);

		if(vals[a] === els[i].value){
			score++;
		} else {
			label.style.color = "red";
		}
	}


	for(var i = 0; i < keys.length; i++){
		//console.log(keys[i]);
		document.querySelector(`[for="${keys[i]}-${vals[i]}"]`).style.color = "green"
	}

	var sd = document.createElement("p");
	sd.innerHTML = `Score: ${score}/${keys.length} (${(score/keys.length).toFixed(2)}%)`;

	document.body.appendChild(sd);
}

function shuf(a){
	var i = a.length;
	var r;

	while(i !=0){
		r = Math.floor(Math.random() * i);
		i--;

		[a[i], a[r]] = [a[r], a[i]];
	}
	return a;
}

function shuffleDivs(){
	var paren = document.getElementById("hamexam");

	//console.log(paren);
	var divarr = Array.prototype.slice.call(paren.getElementsByClassName('q'));

	//console.log(divarr.length);

	for(var i = 0; i < divarr.length; i++){
		paren.removeChild(divarr[i]);
	}

	divarr = shuf(divarr);

	for(var i = 0; i < divarr.length; i++){
		paren.appendChild(divarr[i]);
	}
}

// @license-end
